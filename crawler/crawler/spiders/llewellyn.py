# -*- coding: utf-8 -*-
from urlparse import urlsplit
from urlparse import urlunsplit
from urlparse import parse_qsl
from urllib import urlencode

import scrapy


class LlewellynSpider(scrapy.Spider):
    name = 'llewellyn'
    base_url = 'http://booksellers.llewellyn.com/browse_category.php?sort_by=sales&product_category_id=435'
    custom_settings = {
        'LOG_LEVEL': 'INFO',
        'CONCURRENT_REQUESTS': 1,
        'DOWNLOAD_DELAY': 1,
        'FEED_FORMAT': 'jsonlines',
        'FEED_URI': 'file:///forge/isbndb/isbndb/isbns.jl'
    }

    def __init__(self, *args, **kwargs):
        self.base_url = urlsplit(self.base_url)

    def start_requests(self):
        yield scrapy.Request(url=urlunsplit(self.base_url), callback=self.parse)

    def parse(self, response):
        last_page = int(response.xpath(
            '//div[@class="pagination"][1]/a[last()-1]/text()'
        ).extract_first())
        last_page = ((last_page * 10) - 10) / 10
        for page in range(0, last_page + 1):
            url = self.base_url
            query = parse_qsl(url.query)
            query.append(('start', page * 10))
            new_url = urlunsplit(
                (url.scheme, url.netloc, url.path, urlencode(query), url.fragment)
            )
            yield scrapy.Request(url=new_url, callback=self.parse_page)

    def parse_page(self, response):
        items = response.xpath(
            '//input[@type="hidden"][@name="ean"]/ancestor::tr[1]//td[3]'
        )
        get_value = lambda item, val: item.xpath(val).extract_first()
        for item in items:
            price = get_value(
                item,
                './/td[contains(text(), "Retail:")]/following-sibling::td/text()'
            ).replace('$', '')
            yield {
                'url': response.url.replace('http://booksellers.llewellyn.com/browse_category.php?sort_by=sales&product_category_id=435&start='),
                'title': get_value(item, './/a[@class="book_title"]/text()'),
                'subtitle': get_value(item, './/span[@class="subtitle"]/text()'),
                'isbn': get_value(item, './ancestor::tr[1]//input[@name="ean"]/@value'),
                'price': price
            }
